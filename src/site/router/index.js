import Vue from "vue";
import Router from "vue-router";
// import Home from "@/site/views/Home/Index";

import store from "@/site/store/index";

const Servicios = () => import(/* webpackChunkName: "servicios" */ "@/site/views/Servicios/Index");
const Rental = () => import(/* webpackChunkName: "rental" */ "@/site/views/Rental/Index");
const Landing = () => import(/* webpackChunkName: "landing" */ "@/site/views/Landing/Index");

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "Landing",
      component: Landing
    },
    {
      path: "/rental",
      component: Rental
    },
    {
      path: "/servicios",
      component: Servicios
    },
    {
      path: "*",
      redirect: "/"
    }
  ],
  scrollBehavior(to) {
    if (to.hash) {
      return {
        selector: to.hash
      };
    }
    return {
      x: 0,
      y: 0
    };
  }
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.someProp)) {
    if (!store.state.someObj.someVal) {
      next({
        path: "/"
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
