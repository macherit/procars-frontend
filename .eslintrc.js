module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ["plugin:vue/essential", "@vue/airbnb", "prettier"],
  rules: {
    "prettier/prettier": ["error"],
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    "arrow-parens": 0,
    "array-callback-return": 0,
    "no-nested-ternary": 0,
    "no-underscore-dangle": 0,
    "import/extensions": 0,
    "new-cap": 0,
    "no-mixed-operators": 0,
    "max-len": 0,
    "global-require": 0,
    "import/no-dynamic-require": 0
  },
  parserOptions: {
    parser: "babel-eslint"
  },
  plugins: ["prettier"]
};
